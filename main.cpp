#include <cstring>
#include <cassert>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <functional>
#include <regex>

struct Snapshot
{
    struct Entry
    {
        std::string Price;
        std::string Amount;
    };

    std::vector<std::string> TimeStamps;

    std::string Symbol;

    std::vector<Entry> Bid;
    std::vector<Entry> Offer;

    void ApplyLine(const std::string& aLine, const std::string& aStartTime = "", const std::string& aEndTime = "")
    {
        if (aLine.size() > 10000)
        {
            std::cout << "[Warning]: " << "Too long message: size = " << aLine.size() << "." << std::endl;
            return;
        }

        std::regex mdMessageRegex("^(\\[[\\d\\:\\.]+\\]).*35=([WX])(\\S*\\|55=" + Symbol + "\\S*\\|)10=\\d{3}\\|$");
        std::smatch messageMatch;

        if (std::regex_match(aLine, messageMatch, mdMessageRegex))
        {
            if (!aStartTime.empty() && messageMatch[1] < aStartTime)
            {
                return;
            }

            if (!aEndTime.empty() && messageMatch[1] > aEndTime)
            {
                // TODO: Stop iterating further.
                return;
            }

            TimeStamps.push_back(messageMatch[1]);

            if (messageMatch[2] == "W")
            {
                Bid.clear();
                Offer.clear();

                std::regex snapshotEntryRegex("269=(\\d+)\\|270=([\\d.]*)\\|271=(\\d+)\\|");
                std::smatch entryMatch;

                for (auto subMatch = &messageMatch[3];
                     std::regex_search(subMatch->first, subMatch->second, entryMatch, snapshotEntryRegex);
                     subMatch = &entryMatch.suffix())
                {
                    auto& entriesRef = GetEntriesReference(entryMatch[1]);

                    entriesRef.push_back({entryMatch[2].str(), entryMatch[3].str()});
                }
            }
            else if (messageMatch[2] == "X")
            {
                std::regex snapshotEntryRegex(
                    "279=([012])\\|269=(\\d+)\\|55=" + Symbol + "\\|((?!\\|279=).)*270=([\\d.]*)\\|271=(\\d+)\\|");
                std::smatch entryMatch;

                for (auto subMatch = &messageMatch[3];
                     std::regex_search(subMatch->first, subMatch->second, entryMatch, snapshotEntryRegex);
                     subMatch = &entryMatch.suffix())
                {
                    auto& entriesRef = GetEntriesReference(entryMatch[2]);

                    auto price = entryMatch[4].str();
                    auto entryIter = std::find_if(
                        entriesRef.begin(), entriesRef.end(),
                        [&price](const auto& entry)
                        {
                            return entry.Price == price;
                        }
                    );

                    if (entryMatch[1] == "0")
                    {
                        if (entryIter != entriesRef.end())
                        {
                            entryIter->Amount = entryMatch[5];

//                            throw std::runtime_error("There is an entry with such price already: " + price);
                        }
                        else
                        {
                            entriesRef.push_back({entryMatch[4].str(), entryMatch[5].str()});
                        }
                    }
                    else if (entryMatch[1] == "1")
                    {
                        if (entryIter == entriesRef.end())
                        {
                            std::cout << "[Warning]: " << TimeStamps.back() + " Can't change a quote. No entry with such price " + price + "." << std::endl;
                            continue;
                        }

                        entryIter->Amount = entryMatch[5];
                    }
                    else if (entryMatch[1] == "2")
                    {
                        if (entryIter == entriesRef.end())
                        {
                            std::cout << "[Warning]: " << TimeStamps.back() + " Can't delete a quote. No entry with such price " + price + "." << std::endl;
                            continue;
                        }

                        entriesRef.erase(entryIter);
                    }
                    else
                    {
                        assert(false);
                    }
                }
            }
            else
            {
                assert(false);
            }
        }

        auto EntriesSorter = [](auto comparator, auto& entries)
        {
            std::sort(entries.begin(), entries.end(), [comparator](auto left, auto right)
                {
                    return comparator(left.Price, right.Price);
                }
            );
        };

        EntriesSorter(std::greater_equal<std::string>{}, Bid);
        EntriesSorter(std::less_equal<std::string>{}, Offer);
    }

    void Display()
    {
        if (TimeStamps.empty())
        {
            std::cout << "Empty snapshot." << std::endl;
            return;
        }

        int i = 1;
        std::cout << TimeStamps.back() << std::endl;
        std::cout << "Offer Quotes " << std::endl;
        for(auto& entry : Offer)
        {
            std::cout << "[" << i++ << "] " << "{ " << "Price: " << entry.Price << " Amount: " << entry.Amount << " }" << std::endl;
        }

        i = 1;
        std::cout << "Bid Quotes " << std::endl;
        for(auto& entry : Bid)
        {
            std::cout << "[" << i++ << "] " << "{ " << "Price: " << entry.Price << " Amount: " << entry.Amount << " }" << std::endl;
        }
    }

private:

    decltype(Bid)& GetEntriesReference(const std::string& aEntryType)
    {
        if (aEntryType == "0")
            return Bid;
        else if (aEntryType == "1")
            return Offer;
        else
        {
            throw "Unexpected Entry type";
        }
    }
};


struct Config
{
    static const constexpr char* FixDumpFileConfigName = "--fix-dump=";
    static const constexpr char* SymbolConfigName = "--symbol=";
    static const constexpr char* StartTimeConfigName = "--start-time=";
    static const constexpr char* EndTimeConfigName = "--end-time=";

    std::string FixDumpFile;
    std::string Symbol;
    std::string StartTime;
    std::string EndTime;

    static Config MakeConfig(
        const char* aFirst, const char* aSecond, const char* aThird = nullptr, const char* aFourth = nullptr)
    {
        Config config;

        std::string first(aFirst);
        std::string second(aSecond);

        TrySetConfig(first, FixDumpFileConfigName, &config.FixDumpFile);
        TrySetConfig(second, FixDumpFileConfigName, &config.FixDumpFile);
        TrySetConfig(first, SymbolConfigName, &config.Symbol);
        TrySetConfig(second, SymbolConfigName, &config.Symbol);

        if (!aThird)
            return config;

        std::string third(aThird);

        TrySetConfig(third, StartTimeConfigName, &config.StartTime);
        TrySetConfig(third, EndTimeConfigName, &config.EndTime);

        auto BracketAdder = [&]()
        {
            if (!config.StartTime.empty())
            {
                config.StartTime = "[" + config.StartTime + "]";
            }
            if (!config.EndTime.empty())
            {
                config.EndTime = "[" + config.EndTime + "]";
            }
        };

        BracketAdder();

        if (!aFourth)
            return config;

        std::string fourth(aFourth);
        TrySetConfig(fourth, StartTimeConfigName, &config.StartTime);
        TrySetConfig(fourth, EndTimeConfigName, &config.EndTime);

        BracketAdder();

        return config;
    }

private:

    static void TrySetConfig(std::string aRawParameter, const char* aParameterName, std::string* aParameter)
    {
        if (aRawParameter.find(aParameterName) == 0)
        {
            *aParameter = aRawParameter.substr(strlen(aParameterName));
        }
    }
};


int main(int argc, char *argv[])
{
    Config config;

    // TODO: integral_index?
    switch (argc)
    {
    case 3:
        config = Config::MakeConfig(argv[1], argv[2]);
        break;
    case 4:
        config = Config::MakeConfig(argv[1], argv[2], argv[3]);
        break;
    case 5:
        config = Config::MakeConfig(argv[1], argv[2], argv[3], argv[4]);
        break;

    default:
        std::cout << "Illegal input" << std::endl;
        return EXIT_FAILURE;
    }

    std::ifstream input(config.FixDumpFile);

    if (!input)
    {
        std::cout << "Error opening file: " << config.FixDumpFile << "." << std::endl;
        return EXIT_FAILURE;
    }

    Snapshot snapshot;
    snapshot.Symbol = config.Symbol;

    std::string line;
    while(std::getline(input, line))
    {
        // TODO: Make polymorphic message.
        snapshot.ApplyLine(line, config.StartTime, config.EndTime);
    }

    snapshot.Display();

    return 0;
}
